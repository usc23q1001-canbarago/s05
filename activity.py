from abc import ABC, abstractclassmethod
class Animal(ABC):
	@abstractclassmethod

	def eat(self,food):
		pass

	def make_sound(self):
		pass


class cat(Animal):
	def __init__(self,Name,Breed,Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

	def get_name(self):
		print(f'Name of cat: {self._Name}')

	def get_breed(self):
		print(f'Breed of cat {self._Breed}')

	def get_age(self):
		print(f'Age of cat {self._Age}')


	def set_name(self,name):
		self._Name = Name

	def set_breed(self, Breed):
		self._breed = Breed 

	def set_age(self, Age):
		self._Age = Age

	def eat(self,food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f'{self._Name}, ROR')


	def call(self):
		print(f"{self._Name}, come on")

class dog(Animal):
	def __init__(self,Name,Breed,Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

	def get_name(self):
		print(f'Name of dog: {self._Name}')

	def get_breed(self):
		print(f'Breed of dog {self._Breed}')

	def get_age(self):
		print(f'Age of cat {self._Age}')


	def set_name(self,name):
		self._Name = Name

	def set_breed(self, Breed):
		self._breed = Breed 

	def set_age(self, Age):
		self._Age = Age

	def eat(self,food):
		print(f"eaten {food}")

	def make_sound(self):
		print(f'ROF!!! ROF!!!!')


	def call(self):
		print(f"here {self._Name}")

dog1 = dog("Kiwi", "Pomeranian" , 10)
dog1.eat("wet food")
dog1.make_sound()
dog1.call()
print(" ")
cat1 = dog("Beans","Siamese",1)
cat1.eat("fish")
cat1.make_sound()
cat1.call()



